import { ApplicationRef, Injectable } from '@angular/core';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { catchError, EMPTY, first, retry, switchMap } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class ServiceWorkerUpdaterService {
	constructor(
		private readonly appRef: ApplicationRef, private readonly updates: SwUpdate,
	) {
		this.checkForUpdates();
		this.listenForFoundUpdates();
		this.handleUnrecoverableState();
	}

	private handleUnrecoverableState() {
		this.updates.unrecoverable.subscribe(event => {
			console.log(event);
			document.location.reload();
		});
	}

	private checkForUpdates() {
		const appIsStable$ = this.appRef.isStable.pipe(first(isStable => isStable));

		appIsStable$
			.pipe(
				switchMap(() => this.updates.checkForUpdate()),
				retry(3),
				catchError(error => {
					console.error('Failed to check for updates:', error);
					return EMPTY;
				}),
			)
			.subscribe(updateFound => console.log(updateFound ? 'A new version is available.' : 'Already on the latest version.'));
	}

	private listenForFoundUpdates() {
		this.updates.versionUpdates
			.pipe(filter((evt): evt is VersionReadyEvent => evt.type === 'VERSION_READY'))
			.subscribe(evt => {
				console.log('New version available. Reloading...', evt.currentVersion, evt.latestVersion);
				document.location.reload();
			});
	}
}
