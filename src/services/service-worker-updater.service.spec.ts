import { TestBed } from '@angular/core/testing';

import { ServiceWorkerUpdaterService } from './service-worker-updater.service';
import { SwUpdate } from '@angular/service-worker';
import { EMPTY } from 'rxjs';

describe('ServiceWorkerUpdaterService', () => {
	let service: ServiceWorkerUpdaterService;

	beforeEach(() => {
		const swUpdate = jasmine.createSpyObj<SwUpdate>(
			'SwUpdateMock',
			{
				checkForUpdate: Promise.resolve(false),
			},
			{
				versionUpdates: EMPTY,
				unrecoverable: EMPTY,
			},
		);

		TestBed.configureTestingModule({
			providers: [
				{
					provide: SwUpdate,
					useValue: swUpdate,
				},
			],
		});
		service = TestBed.inject(ServiceWorkerUpdaterService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
