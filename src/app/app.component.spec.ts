import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
	let fixture: ComponentFixture<AppComponent>;
	let app: AppComponent;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [
				AppComponent,
			],
		}).compileComponents();
	});

	it('should create the app', () => {
		fixture = TestBed.createComponent(AppComponent);
		app = fixture.componentInstance;
		expect(app).toBeTruthy();
	});
});
