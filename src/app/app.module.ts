import { APP_INITIALIZER, ApplicationRef, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { isNotNil } from '../shared/model/nil';
import { ServiceWorkerUpdaterService } from '../services/service-worker-updater.service';
import { ServiceWorkerModule, SwUpdate } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		NgbModule,
		RouterModule.forRoot([
			{
				path: '',
				loadChildren: () => import('../modules/home/home.module').then(m => m.HomeModule),
			},
			{
				path: 'memory',
				loadChildren: () => import('../modules/memory-game/memory-game.module').then(m => m.MemoryGameModule),
			},
			{
				path: 'logic-flow',
				loadChildren: () => import('../modules/logic-flow/logic-flow.module').then(m => m.LogicFlowModule),
			},
			{
				path: 'rock-paper-scissors',
				loadChildren: () => import('../modules/rock-paper-scissors/rock-paper-scissors.module').then(m => m.RockPaperScissorsModule),
			},
		]),
		ServiceWorkerModule.register('ngsw-worker.js', {
			enabled: environment.production,
			registrationStrategy: 'registerWhenStable:30000',
		}),
	],
	providers: [
		environment.production ? {
			provide: APP_INITIALIZER,
			multi: true,
			useFactory: (service: ServiceWorkerUpdaterService) => () => undefined,
			deps: [ApplicationRef, SwUpdate, ServiceWorkerUpdaterService],
		} : null,
	].filter(isNotNil),
	bootstrap: [AppComponent],
})
export class AppModule {
}
