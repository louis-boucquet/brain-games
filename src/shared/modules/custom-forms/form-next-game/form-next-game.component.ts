import { Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { WithLifecycleHooks } from '../../../with-lifecycle-hooks.component';
import { FormControl } from '@angular/forms';
import { delay, filter, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
	selector: 'form[app-form-next-game]',
	templateUrl: './form-next-game.component.html',
	styleUrls: ['./form-next-game.component.css'],
})
export class FormNextGameComponent extends WithLifecycleHooks {
	@Input()
	gameRunning!: boolean;
	@Input()
	delay = 1000;
	@Input()
	autoNextDefault = true;

	@Output()
	readonly next = new EventEmitter<void>();

	@HostBinding('class')
	readonly hostClass = 'd-flex justify-content-between w-100 my-2';

	readonly formControl = new FormControl<boolean>(
		true,
		{ nonNullable: true },
	);

	constructor() {
		super();

		this.initNextListener();
		this.initPatchListener();
	}

	private initNextListener() {
		this.onChanges$
			.pipe(
				filter(() => !this.gameRunning),
				filter(() => this.formControl.value),
				switchMap(() => of(null).pipe(delay(this.delay))),
			)
			.subscribe(() => setTimeout(() => this.next.emit(), this.delay));
	}

	private initPatchListener() {
		this.onInit$
			.subscribe(
				() => this.formControl.patchValue(this.autoNextDefault),
			);
	}
}
