import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNextGameComponent } from './form-next-game.component';

describe('FormNextGameComponent', () => {
	let component: FormNextGameComponent;
	let fixture: ComponentFixture<FormNextGameComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [FormNextGameComponent],
		})
			.compileComponents();

		fixture = TestBed.createComponent(FormNextGameComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
