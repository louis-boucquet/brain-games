import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormNextGameComponent } from './form-next-game/form-next-game.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
	declarations: [
		FormNextGameComponent,
	],
	exports: [
		FormNextGameComponent,
	],
	imports: [
		CommonModule,
		ReactiveFormsModule,
	],
})
export class CustomFormsModule {
}
