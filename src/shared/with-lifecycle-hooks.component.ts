import { Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Component({
	template: '',
})
export class WithLifecycleHooks implements OnInit, OnChanges, OnDestroy {
	readonly onInit$ = new Subject<void>();
	readonly onChanges$ = new Subject<void>();
	readonly onDestroy$ = new Subject<void>();

	ngOnInit() {
		this.onInit$.next();
	}

	ngOnChanges() {
		this.onChanges$.next();
	}

	ngOnDestroy() {
		this.onDestroy$.next();

		this.onInit$.complete();
		this.onChanges$.complete();
		this.onDestroy$.complete();
	}
}
