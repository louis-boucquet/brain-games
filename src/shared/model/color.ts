export const colors = [
	'red',
	'green',
	'yellow',
	'orange',
	'blue',
	'purple',
	'turquoise',
	'pink',
	'brown',
] as const;

export type Color = typeof colors[number];
