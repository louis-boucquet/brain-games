export const directions = [
	'left',
	'right',
	'up',
	'down',
] as const;

export type Direction = typeof directions[number];

export function getDirectionFromNormalizedDirection(xDiff: number, yDiff: number) {
	if (xDiff === 1 && yDiff === 0) return 'right';
	if (xDiff === -1 && yDiff === 0) return 'left';
	if (xDiff === 0 && yDiff === 1) return 'down';
	if (xDiff === 0 && yDiff === -1) return 'up';

	throw new Error('Invalid coordinate diff');
}

export function getCoordinatesFromDirection(next: Direction) {
	return {
		left: { x: -1, y: 0 },
		right: { x: 1, y: 0 },
		up: { x: 0, y: -1 },
		down: { x: 0, y: 1 },
	}[next];

}

export function getOppositeDirection(direction: Direction) {
	return {
		up: 'down',
		down: 'up',
		left: 'right',
		right: 'left',
	}[direction];
}
