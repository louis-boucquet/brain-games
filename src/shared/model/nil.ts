export type Nil = null | undefined;

export function isNotNil<T>(value: T | Nil): value is T {
	return value !== undefined && value !== null;
}
