export type Coordinate = { x: number, y: number };

export function createRandomCoordinate(size: Coordinate) {
	return {
		x: Math.floor(Math.random() * size.x),
		y: Math.floor(Math.random() * size.y),
	};
}

export function add(cA: Coordinate, cB: Coordinate) {
	return {
		x: cA.x + cB.x,
		y: cA.y + cB.y,
	};
}

export function subtract(cA: Coordinate, cB: Coordinate) {
	return {
		x: cA.x - cB.x,
		y: cA.y - cB.y,
	};
}

export function checkBounds(size: Coordinate, coordinate: Coordinate) {
	return coordinate.x >= 0 && coordinate.x < size.x
		&& coordinate.y >= 0 && coordinate.y < size.y;
}

export function coordinateEquals(a: Coordinate, b: Coordinate) {
	return a.x === b.x && a.y === b.y;
}

export function isCoordinate(value: unknown): value is Coordinate {
	return typeof value === 'object' &&
		value !== null &&
		'x' in value &&
		'y' in value &&
		typeof (value as Partial<Coordinate>).x === 'number' &&
		typeof (value as Partial<Coordinate>).y === 'number' &&
		!isNaN((value as Coordinate).x) &&
		!isNaN((value as Coordinate).y);
}
