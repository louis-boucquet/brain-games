export function wait(ms: number): Promise<void> {
	return new Promise(resolve => setTimeout(resolve, ms));
}

export function limitPromise<PromiseValue, DefaultValue>(
	getPromise: (ab: AbortController) => Promise<PromiseValue>,
	defaultValue: PromiseValue,
	ms: number,
): Promise<PromiseValue | DefaultValue> {
	const abortController = new AbortController();

	return Promise.race([
		getPromise(abortController),
		wait(ms).then(() => {
			abortController.abort();
			return defaultValue;
		}),
	]);
}

/**
 * Takes a synchronous promise and makes it async
 * @param getPromise
 */
export function makeAsync<T>(
	getPromise: () => Promise<T>,
): Promise<T> {
	return new Promise<T>(resolve => setTimeout(async () => resolve(await getPromise()), 0));
}
