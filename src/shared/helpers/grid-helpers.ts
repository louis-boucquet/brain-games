import { Coordinate } from '../model/coordinate';

export function createGrid<T>(size: Coordinate, valueGetter: (coordinate: Coordinate) => T): T[][] {
	const grid: T[][] = [];
	for (let y = 0; y < size.y; y++) {
		const row: T[] = [];

		for (let x = 0; x < size.x; x++)
			row.push(valueGetter({ x, y }));

		grid.push(row);
	}
	return grid;
}
