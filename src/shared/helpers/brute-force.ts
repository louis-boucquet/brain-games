import { limitPromise, makeAsync } from './timeout';

export interface Action<State> {
	execute(state: State): void;

	revert(state: State): void;
}

function solveWithBruteForceNoneLimited<State>(
	state: State,
	getActions: (state: State) => Action<State>[],
	isSolved: (state: State) => boolean,
	canSnip: (state: State) => boolean,
	abortController: AbortController,
): Promise<Action<State>[] | null> {
	return makeAsync(async () => {
		if (abortController.signal.aborted)
			return null;

		if (isSolved(state))
			return [];

		if (canSnip(state))
			return null;

		const actions = getActions(state);

		if (actions.length === 0)
			return null;

		for (const action of actions) {
			action.execute(state);

			const actionsOrNull = await solveWithBruteForceNoneLimited(
				state,
				getActions,
				isSolved,
				canSnip,
				abortController,
			);

			action.revert(state);

			if (abortController.signal.aborted)
				return null;

			if (actionsOrNull !== null)
				return [action, ...actionsOrNull];
		}

		return null;
	});
}

export function solveWithBruteForce<State>(
	state: State,
	getActions: (state: State) => Action<State>[],
	isSolved: (state: State) => boolean,
	canSnip: (state: State) => boolean,
	ms: number = 1000,
): Promise<Action<State>[] | null> {
	return limitPromise(
		abortController => solveWithBruteForceNoneLimited(state, getActions, isSolved, canSnip, abortController),
		null,
		ms,
	);
}
