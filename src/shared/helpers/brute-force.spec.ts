import { Action, solveWithBruteForce } from './brute-force';

describe('brute-force', () => {
	describe('solveWithBruteForce', () => {
		type SimpleTestState = { num: number };

		const state = { num: 0 };
		let actions: Action<SimpleTestState>[] | null;

		beforeEach(async () => {
			actions = await solveWithBruteForce(
				state,
				(state: SimpleTestState) => state.num < 6 ? [
					{
						execute: (state: SimpleTestState) => state.num += 2,
						revert: (state: SimpleTestState) => state.num -= 2,
					},
					{
						execute: (state: SimpleTestState) => state.num += 1,
						revert: (state: SimpleTestState) => state.num -= 1,
					},
				] : [],
				(state: SimpleTestState) => state.num === 5,
				() => false,
			);
		});

		it('Should solve a simple problem', () => {
			expect(actions).not.toBeNull();
			expect(actions?.length).toBe(3);
		});

		it('Should reset the state', () => {
			expect(state.num).toBe(0);
		});
	});
});
