import { Timer } from './time-function';

describe('Timer', () => {
	beforeEach(() => Timer.runningTimers.clear());

	it('should work', () => {
		const timer = new Timer('test');
		timer.start();
		timer.end();
	});

	it('should work twice', () => {
		const timer = new Timer('test');
		timer.start();
		timer.end();

		timer.start();
		timer.end();
	});

	it('should throw if end is called before start', () => {
		const timer = new Timer('test');
		expect(() => timer.end()).toThrow();
	});

	it('should throw if end is called twice', () => {
		const timer = new Timer('test');
		timer.start();
		timer.end();
		expect(() => timer.end()).toThrow();
	});

	it('should throw if start is called twice', () => {
		const timer = new Timer('test');
		timer.start();
		expect(() => timer.start()).toThrow();
	});

	it('should update the running timers set', () => {
		const timer = new Timer('test');

		timer.start();
		expect(Timer.runningTimers.has('test')).toBeTrue();
		timer.end();
		expect(Timer.runningTimers.has('test')).toBeFalse();
	});

	it('should work with multiple timers', () => {
		const timer1 = new Timer('test');
		const timer2 = new Timer('test');
		timer1.start();
		timer2.start();
		timer1.end();
		timer2.end();
	});

	it('should give unique names to timers with the same name', () => {
		const timer1 = new Timer('test');
		const timer2 = new Timer('test');
		timer1.start();
		timer2.start();
		expect(Timer.runningTimers.has('test')).toBeTrue();
		expect(Timer.runningTimers.has('test (1)')).toBeTrue();
	});
});
