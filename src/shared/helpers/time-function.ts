export class Timer {
	static readonly runningTimers = new Set<string>();

	private uniqueName: string | null = null;

	constructor(private readonly name: string) {
		this.start = this.start.bind(this);
		this.end = this.end.bind(this);
	}

	start() {
		if (this.uniqueName !== null)
			throw new Error(`Trying to start Timer "${this.uniqueName}" but it was already running`);

		this.uniqueName = this.getUniqueName();
		Timer.runningTimers.add(this.uniqueName);
		console.time(this.uniqueName);
	}

	end() {
		if (this.uniqueName === null)
			throw new Error(`Trying to end Timer "${this.uniqueName}" but it wasn't running`);

		console.timeEnd(this.uniqueName);
		Timer.runningTimers.delete(this.uniqueName);
		this.uniqueName = null;
	}

	private getUniqueName() {
		if (!Timer.runningTimers.has(this.name))
			return this.name;

		let count = 1;
		let nameWithCount: string;

		do nameWithCount = `${this.name} (${count++})`;
		while (Timer.runningTimers.has(nameWithCount));

		return nameWithCount;
	}
}

export function TimeFunction() {
	return function (
		target: any,
		propertyKey: string,
		descriptor: PropertyDescriptor,
	) {
		const originalMethod = descriptor.value;

		descriptor.value = function (...args: any[]) {
			const timer = new Timer(propertyKey);

			timer.start();

			const returnValue = originalMethod.apply(this, args);

			if (returnValue instanceof Promise) returnValue.then(timer.end);
			else timer.end();

			return returnValue;
		};
	};
}
