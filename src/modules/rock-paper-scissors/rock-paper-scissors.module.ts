import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { RockPaperScissorsComponent } from './rock-paper-scissors.component';

@NgModule({
	declarations: [
		RockPaperScissorsComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: RockPaperScissorsComponent,
			},
		]),
	],
})
export class RockPaperScissorsModule {
}
