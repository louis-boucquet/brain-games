import { Component } from '@angular/core';

const options = [
	'rock',
	'paper',
	'scissors',
] as const;

type Option = typeof options[number]

@Component({
	selector: 'app-rock-paper-scissors',
	templateUrl: './rock-paper-scissors.component.html',
	styleUrls: ['./rock-paper-scissors.component.css'],
})
export class RockPaperScissorsComponent {
	numWins = 0;
	numLosses = 0;
	lastPlayerOptions: Option | null = null;
	lastAiOptions: Option | null = null;
	private readonly winsFromTable: Record<Option, Option> = {
		rock: 'scissors',
		paper: 'rock',
		scissors: 'paper',
	};
	private readonly losesByTable: Record<Option, Option> = {
		rock: 'paper',
		paper: 'scissors',
		scissors: 'rock',
	};
	private readonly optionFrequencies: Record<Option, number> = {
		rock: 0,
		paper: 0,
		scissors: 0,
	};

	get winRate(): string {
		const numDecisiveRounds = this.numWins + this.numLosses;

		if (numDecisiveRounds === 0) return '-';

		return `${(100 * (this.numWins / numDecisiveRounds)).toFixed(0)} %`;
	}

	selectOption(option: Option): void {
		this.optionFrequencies[option]++;

		const aiOption = this.getAIOption();

		this.lastPlayerOptions = option;
		this.lastAiOptions = aiOption;

		if (option === aiOption)
			return;

		if (this.winsFromTable[option] === aiOption)
			this.numWins++;
		else if (this.winsFromTable[aiOption] === option)
			this.numLosses++;
	}

	private getAIOption() {
		const userInput = this.predictUserInput();
		return this.losesByTable[userInput];
	}

	private predictUserInput(): Option {
		return Object
			.entries(this.optionFrequencies)
			.map(([option, frequency]) => ({
				option,
				frequency,
			}))
			.reduce((previous, current) =>
				current.frequency > previous.frequency
					? current
					: previous,
			)?.option as Option;
	}
}
