import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogicFlowComponent } from './logic-flow.component';
import { RouterModule } from '@angular/router';
import { LogicFlowBoardComponent } from './logic-flow-board/logic-flow-board.component';
import { NgbAccordionModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from '../../shared/modules/custom-forms/custom-forms.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
	declarations: [
		LogicFlowComponent,
		LogicFlowBoardComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: LogicFlowComponent,
			},
		]),
		NgbAccordionModule,
		CustomFormsModule,
		ReactiveFormsModule,
		NgbNavModule,
	],
})
export class LogicFlowModule {
}
