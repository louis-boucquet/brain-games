import { Coordinate } from '../../shared/model/coordinate';

export type Preset = {
	title: string;
	settings: {
		size: Coordinate,
		numColors: number,
		numDestinationTiles: number,
		numBlockedTiles: number,
	}
}

export const presets: Preset[] = [
	{
		title: 'Tiny',
		settings: {
			size: { x: 3, y: 3 },
			numColors: 2,
			numDestinationTiles: 2,
			numBlockedTiles: 2,
		},
	},
	{
		title: 'Small',
		settings: {
			size: { x: 3, y: 5 },
			numColors: 2,
			numDestinationTiles: 2,
			numBlockedTiles: 3,
		},
	},
	{
		title: 'Medium',
		settings: {
			size: { x: 5, y: 5 },
			numColors: 3,
			numDestinationTiles: 2,
			numBlockedTiles: 2,
		},
	},
	{
		title: 'Big',
		settings: {
			size: { x: 8, y: 6 },
			numColors: 9,
			numDestinationTiles: 6,
			numBlockedTiles: 6,
		},
	},
];
