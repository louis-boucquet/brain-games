import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Coordinate, coordinateEquals, isCoordinate } from '../../../shared/model/coordinate';
import { WithLifecycleHooks } from '../../../shared/with-lifecycle-hooks.component';
import {
	BehaviorSubject,
	combineLatest,
	distinctUntilChanged,
	firstValueFrom,
	map,
	shareReplay,
	Subject,
	switchMap,
	takeUntil,
	tap,
} from 'rxjs';
import { TimeFunction } from '../../../shared/helpers/time-function';
import { delay, filter } from 'rxjs/operators';
import { isNotNil } from '../../../shared/model/nil';
import { generateBoard } from '../logic/generate-board';

@Component({
	selector: 'app-logic-flow-board',
	templateUrl: './logic-flow-board.component.html',
	styleUrls: ['./logic-flow-board.component.scss'],
})
export class LogicFlowBoardComponent extends WithLifecycleHooks {
	@Input() size!: Coordinate;
	@Input() numColors!: number;
	@Input() numBlockedTiles!: number;
	@Input() numDestinationTiles!: number;

	@Output()
	readonly onFinish = new EventEmitter<void>();

	readonly loading$ = new BehaviorSubject(true);
	readonly toucheMove$ = new Subject<TouchEvent>();

	readonly loadingLong$ = combineLatest([
		this.loading$,
		this.loading$.pipe(delay(2000)),
	]).pipe(map(([loading, loadingLong]) => loading && loadingLong));

	readonly board$ = this.onChanges$
		.pipe(
			tap(() => this.loading$.next(true)),
			switchMap(() => this.getBoard()),
			tap(() => this.loading$.next(false)),
			takeUntil(this.onDestroy$),
			shareReplay(1),
		);

	constructor() {
		super();

		this.board$.subscribe();

		this.initTouchMoveListener();
	}

	@TimeFunction()
	async startPath(coordinate: Coordinate) {
		(await firstValueFrom(this.board$))
			.startPath(coordinate);
	}

	@TimeFunction()
	async continuePath(coordinate: Coordinate) {
		const board = await firstValueFrom(this.board$);
		board.continuePath(coordinate);

		if (board.isFinished())
			this.onFinish.emit();
	}

	@TimeFunction()
	async endPath() {
		(await firstValueFrom(this.board$))
			.endPath();
	}

	@TimeFunction()
	private async getBoard() {
		return generateBoard({
			size: this.size,
			numDestinationTiles: this.numDestinationTiles,
			numColors: this.numColors,
			numBlockedTiles: this.numBlockedTiles,
		});
	}

	private initTouchMoveListener() {
		this.toucheMove$
			.pipe(
				map(event => event.touches[0]),
				map(touch => document.elementFromPoint(touch.clientX, touch.clientY)),
				filter(isNotNil),
				filter((element): element is HTMLElement => element instanceof HTMLElement),
				map(element => ({
					x: parseFloat(element.dataset['x'] ?? ''),
					y: parseFloat(element.dataset['y'] ?? ''),
				})),
				filter(isCoordinate),
				distinctUntilChanged(coordinateEquals),
			)
			.subscribe(coordinate => this.continuePath(coordinate));
	}
}
