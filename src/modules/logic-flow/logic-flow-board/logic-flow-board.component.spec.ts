import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LogicFlowBoardComponent } from './logic-flow-board.component';

describe('LogicFlowBoardComponent', () => {
	let component: LogicFlowBoardComponent;
	let fixture: ComponentFixture<LogicFlowBoardComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [LogicFlowBoardComponent],
		}).compileComponents();

		fixture = TestBed.createComponent(LogicFlowBoardComponent);
		component = fixture.componentInstance;
		component.size = { x: 3, y: 3 };
		component.ngOnChanges();
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
