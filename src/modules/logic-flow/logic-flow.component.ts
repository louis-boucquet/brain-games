import { ChangeDetectorRef, Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { WithLifecycleHooks } from '../../shared/with-lifecycle-hooks.component';
import { Preset, presets } from './presets';

@Component({
	selector: 'app-logic-flow',
	templateUrl: './logic-flow.component.html',
	styleUrls: ['./logic-flow.component.css'],
})
export class LogicFlowComponent extends WithLifecycleHooks {
	readonly formGroup = new FormGroup(
		{
			size: new FormGroup({
				x: new FormControl(
					3,
					{
						nonNullable: true,
						validators: [Validators.required, Validators.min(2), Validators.max(10)],
					},
				),
				y: new FormControl(
					3,
					{
						nonNullable: true,
						validators: [Validators.required, Validators.min(2), Validators.max(10)],
					},
				),
			}),
			numColors: new FormControl(
				3,
				{
					nonNullable: true,
					validators: [Validators.required, Validators.min(1), Validators.max(9)],
				},
			),
			numBlockedTiles: new FormControl(
				3,
				{
					nonNullable: true,
					validators: [Validators.required, Validators.min(0)],
				},
			),
			numDestinationTiles: new FormControl(
				3,
				{
					nonNullable: true,
					validators: [Validators.required, Validators.min(0)],
				},
			),
		},
		{
			validators: [
				this.getSizeValidator(),
				this.getTooManyDestinationsValidator(),
			],
		},
	);

	readonly presets: Preset[] = presets;

	size = { x: 3, y: 3 };
	numColors = 3;
	numDestinationTiles = 2;
	numBlockedTiles = 3;

	readonly gameRunning$ = new BehaviorSubject(true);

	constructor(private readonly cd: ChangeDetectorRef) {
		super();

		this.initPatchFirstPreset();
	}

	submit() {
		if (this.formGroup.invalid)
			return;

		this.next();
	}

	onFinish() {
		this.gameRunning$.next(false);
	}

	next() {
		const { x, y } = this.formGroup.getRawValue().size;
		this.size = { x, y };
		this.numColors = this.formGroup.getRawValue().numColors;
		this.numBlockedTiles = this.formGroup.getRawValue().numBlockedTiles;
		this.numDestinationTiles = this.formGroup.getRawValue().numDestinationTiles;

		this.cd.markForCheck();

		this.gameRunning$.next(true);
	}

	nextPreset(settings: Preset['settings']) {
		this.formGroup.patchValue(settings);
		this.next();
	}

	private getSizeValidator(): ValidatorFn {
		return formGroup => {
			const value = (formGroup as typeof this.formGroup).getRawValue();

			const totalNeededTiles = value.numColors + value.numBlockedTiles + value.numDestinationTiles;

			return value.size.x * value.size.y < totalNeededTiles
				? { invalidSize: true }
				: null;
		};
	}

	private getTooManyDestinationsValidator(): ValidatorFn {
		return formGroup => {
			const value = (formGroup as typeof this.formGroup).getRawValue();

			return value.numDestinationTiles > value.numColors
				? { tooManyDestinations: true }
				: null;
		};
	}

	private initPatchFirstPreset() {
		this.onInit$.subscribe(() => this.nextPreset(this.presets[0].settings));
	}
}
