import { add, checkBounds, Coordinate, subtract } from '../../../shared/model/coordinate';
import {
	Direction,
	directions,
	getCoordinatesFromDirection,
	getDirectionFromNormalizedDirection,
	getOppositeDirection,
} from '../../../shared/model/direction';
import { Color } from '../../../shared/model/color';

export type TileEmpty = {
	type: 'empty',
	// Group nullable values into a `pathState` field
	color: Color | null,
	next: Direction | null,
	length: number | null,
};

export type TileSource = {
	type: 'source',
	color: Color,
	next: Direction | null,
	length: number,
};

export type TileBlocked = {
	type: 'blocked',
};

export type TileDestination = {
	type: 'destination',
	color: Color,
	satisfied: boolean,
};

export type Tile =
	| TileEmpty
	| TileSource
	| TileBlocked
	| TileDestination;
// | 'directional'
// | 'color-restricted'
// | 'refill'
// | 'crossing'

export class LogicFlowBoard {
	private currentPathEnd: Coordinate | null = null;

	constructor(
		readonly grid: Tile[][],
		readonly size: Coordinate,
	) {
	}

	startPath(coordinate: Coordinate) {
		if (!this.canStartPath(coordinate))
			return;

		this.breakExistingPath(coordinate);

		this.currentPathEnd = coordinate;
	}

	continuePath(coordinate: Coordinate) {
		if (this.currentPathEnd === null) return;

		const direction = this.getDirectionIfConnected(this.currentPathEnd, coordinate);
		if (direction === null) return;

		const currentTile = this.currentPathEndTile;
		const targetTile = this.getTile(coordinate);

		if (!this.canMove(currentTile, targetTile)) return;

		if (
			currentTile.type === 'source' ||
			currentTile.type === 'empty'
		) currentTile.next = direction;

		if (targetTile.type === 'destination')
			targetTile.satisfied = true;
		if (targetTile.type === 'empty') {
			targetTile.color = currentTile.color;

			if ('length' in currentTile) {
				if (currentTile.length === null)
					throw new Error('`length` is `null`');

				targetTile.length = currentTile.length - 1;
			}
		}

		this.currentPathEnd = coordinate;
	}

	endPath() {
		this.currentPathEnd = null;
	}

	isFinished(): boolean {
		return this.grid.flat()
			.every(tile =>
				tile.type === 'source' ||
				(tile.type === 'empty' && tile.color !== null) ||
				(tile.type === 'destination' && tile.satisfied) ||
				tile.type === 'blocked',
			);
	}

	getAvailableMoves(): ({ from: Coordinate, to: Coordinate })[] {
		return this.grid
			.flatMap((row, y) => row.map((tile, x) => ({ tile, coordinate: { x, y } })))
			.filter(tileWrapper =>
				tileWrapper.tile.type !== 'blocked' &&
				tileWrapper.tile.type !== 'destination' &&
				tileWrapper.tile.color !== null &&
				tileWrapper.tile.next === null)
			.flatMap(tileWrapper => directions
				.map(getCoordinatesFromDirection)
				.map(direction => add(tileWrapper.coordinate, direction))
				.filter(coordinate => checkBounds(this.size, coordinate))
				.filter(coordinate => this.canMove(tileWrapper.tile, this.getTile(coordinate)))
				.map(targetCoordinate => ({
					from: tileWrapper.coordinate,
					to: targetCoordinate,
				})),
			);
	}

	canSnip() {
		return this.grid
			.flatMap((row, y) => row.map((tile, x) => ({ tile, coordinate: { x, y } })))
			.filter(tileWrapper =>
				tileWrapper.tile.type !== 'blocked' &&
				tileWrapper.tile.color === null)
			.some(tileWrapper =>
				directions
					.map(direction => [direction, add(tileWrapper.coordinate, getCoordinatesFromDirection(direction))] as const)
					.filter(([, coordinate]) => checkBounds(this.size, coordinate))
					.map(([direction, coordinate]) => [direction, this.getTile(coordinate)] as const)
					.every(([direction, tile]) =>
						tile.type === 'blocked' ||
						tile.type === 'destination' ||
						tile.next !== null &&
						direction !== getOppositeDirection(tile.next)),
			);
	}

	getRestingPathLength(color: Color): number {
		const tile = this.grid
			.flatMap(row => row)
			.find((tile): tile is TileSource | TileDestination | TileEmpty =>
				(tile.type === 'destination' && tile.color === color && tile.satisfied) ||
				(tile.type === 'source' && tile.color === color && tile.next === null) ||
				(tile.type === 'empty' && tile.color === color && tile.next === null),
			);

		if (tile == null)
			throw new Error(`Could not find a path with color '${color}'`);

		if (tile.type === 'destination')
			return 0;

		return tile.length!;
	}

	private get currentPathEndTile() {
		if (this.currentPathEnd === null)
			throw new Error('`currentPathEnd` is `null`');

		const tile = this.getTile(this.currentPathEnd);

		if (tile.type === 'blocked')
			throw new Error('Current path end is type `TileBlocked`');

		return tile;
	}

	private canMove(from: Tile, to: Tile) {
		if (
			from.type === 'blocked' ||
			from.type === 'destination'
		) return false;
		if (
			to.type === 'blocked' ||
			to.type === 'source'
		) return false;

		if (from.length !== null && from.length < 1)
			return false;
		if (to.type === 'destination' && from.length !== 1)
			return false;

		if (
			to.type === 'empty' &&
			to.color !== null
		) return false;
		if (
			to.type === 'destination' &&
			'color' in from &&
			to.color !== from.color
		) return false;

		return true;
	}

	private getTile(coordinate: Coordinate) {
		return this.grid[coordinate.y][coordinate.x];
	}

	private canStartPath(coordinate: Coordinate) {
		const tile = this.getTile(coordinate);
		return tile.type !== 'blocked' &&
			tile.type !== 'destination' &&
			tile.color !== null;
	}

	private breakExistingPath(coordinate: Coordinate) {
		const [first, ...restOfPath] = this.iteratePath(coordinate);

		if (first.type === 'blocked') return;
		if (first.type === 'destination') return;

		first.next = null;

		restOfPath.forEach(tile => {
			if (tile.type === 'blocked') return;
			else if (tile.type === 'destination') {
				tile.satisfied = false;
			} else if (
				tile.type === 'source' ||
				tile.type === 'empty'
			) {
				tile.next = null;
				tile.color = null;
				tile.length = null;
			}
		});
	}

	private getDirectionIfConnected(currentPathEnd: Coordinate, coordinate: Coordinate): Direction | null {
		const diff = subtract(coordinate, currentPathEnd);

		if (diff.x === 0 && diff.y === 0)
			return null;
		if (Math.abs(diff.x) > 1 || Math.abs(diff.y) > 1)
			return null;

		try {
			return getDirectionFromNormalizedDirection(diff.x, diff.y);
		} catch (e) {
			return null;
		}
	}

	private * iteratePath(coordinate: Coordinate): Iterable<Tile> {
		let tile: Tile = this.getTile(coordinate);

		while (
			tile.type !== 'blocked' &&
			tile.type !== 'destination' &&
			tile.next !== null) {
			yield tile;

			const directionCoordinate = getCoordinatesFromDirection(tile.next);
			coordinate = add(coordinate, directionCoordinate);
			tile = this.getTile(coordinate);
		}

		yield tile;
	}
}
