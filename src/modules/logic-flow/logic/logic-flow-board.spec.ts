import { LogicFlowBoard, Tile, TileSource } from './logic-flow-board';
import { createGrid } from '../../../shared/helpers/grid-helpers';
import { solveWithBruteForce } from '../../../shared/helpers/brute-force';

describe('LogicFlowBoard', () => {
	let board: LogicFlowBoard;

	function solveBoard() {
		return solveWithBruteForce(
			board,
			board => board.getAvailableMoves()
				.map(({ from, to }) => ({
					execute(board: LogicFlowBoard) {
						board.startPath(from);
						board.continuePath(to);
						board.endPath();
					},
					revert(board: LogicFlowBoard) {
						board.startPath(from);
						board.endPath();
					},
				})),
			board => board.isFinished(),
			board => board.canSnip(),
		);
	}

	describe('with 3x3 grid', () => {
		const size = { x: 3, y: 3 };

		beforeEach(() => {
			const grid = createGrid<Tile>(size, () => ({
				type: 'empty',
				color: null,
				next: null,
				length: null,
			}));

			grid[0][0] = {
				type: 'source',
				color: 'red',
				next: null,
				length: 4,
			};

			grid[2][2] = {
				type: 'source',
				color: 'green',
				next: null,
				length: 2,
			};

			grid[2][0] = {
				type: 'destination',
				color: 'red',
				satisfied: false,
			};

			grid[1][0] = {
				type: 'blocked',
			};

			/*
			 * Makes the following grid:
			 * r = Red Source tile
			 * g = Green Source tile
			 * b = Blocked tile
			 * dr = Red Destination tile
			 *
			 * [r ]   [  ]  [  ]
			 *
			 * [b ]   [  ]  [  ]
			 *
			 * [dr]   [  ]  [g ]
			 */
			board = new LogicFlowBoard(grid, size);
		});

		function withCorrectRedPath() {
			board.startPath({ x: 0, y: 0 });
			board.continuePath({ x: 1, y: 0 });
			board.continuePath({ x: 1, y: 1 });
			board.continuePath({ x: 1, y: 2 });
			board.continuePath({ x: 0, y: 2 });
			board.endPath();
		}

		it('should have been created', () => {
			expect(board).toBeTruthy();
		});

		it('should solve the board', async () =>
			expect(await solveBoard()).not.toBeNull());

		describe('getRestingPathLength', () => {
			it('should return the original path length on start', () => {
				const length = board.getRestingPathLength('green');

				expect(length).toBe(2);
			});

			it('should return 0 for a completed path', () => {
				withCorrectRedPath();

				const length = board.getRestingPathLength('red');
				expect(length).toBe(0);
			});

			it('should throw for absent colors', () => {
				expect(() => board.getRestingPathLength('blue')).toThrow();
			});

			it('should not pick an unsatisfied destination', () => {
				board.grid[0][1] = {
					type: 'destination',
					color: 'green',
					satisfied: false,
				};

				const length = board.getRestingPathLength('green');
				expect(length).toBe(2);
			});
		});

		describe('paths', () => {
			it('should draw from source', () => {
				board.startPath({ x: 0, y: 0 });
				board.continuePath({ x: 1, y: 0 });
				board.endPath();

				expect(board.grid[0][0])
					.toEqual(jasmine.objectContaining({ next: 'right' }));
			});

			it('should not draw from an empty tile', () => {
				board.startPath({ x: 1, y: 1 });
				board.continuePath({ x: 1, y: 0 });
				board.endPath();

				expect(board.grid[1][1])
					.toEqual(jasmine.objectContaining({ next: null }));
			});

			it('should not draw diagonally', () => {
				board.startPath({ x: 0, y: 0 });
				board.continuePath({ x: 1, y: 1 });
				board.endPath();

				expect(board.grid[0][0])
					.toEqual(jasmine.objectContaining({ next: null }));
			});

			it('should not draw from a TileBlocked', () => {
				board.startPath({ x: 0, y: 1 });
				board.continuePath({ x: 1, y: 1 });
				board.endPath();

				expect(board.grid[1][0])
					.toEqual(jasmine.objectContaining({ type: 'blocked' }));
			});

			it('should not draw over a TileBlocked', () => {
				board.startPath({ x: 0, y: 0 });
				board.continuePath({ x: 0, y: 1 });
				board.endPath();

				expect(board.grid[0][0])
					.toEqual(jasmine.objectContaining({ next: null }));
			});

			it('should draw from existing path', () => {
				board.startPath({ x: 0, y: 0 });
				board.continuePath({ x: 1, y: 0 });
				board.continuePath({ x: 2, y: 0 });
				board.endPath();

				board.startPath({ x: 1, y: 0 });
				board.continuePath({ x: 1, y: 1 });
				board.endPath();

				expect(board.grid[0][1])
					.toEqual(jasmine.objectContaining({ next: 'down' }));
			});

			it('should draw from existing path end', () => {
				board.startPath({ x: 0, y: 0 });
				board.continuePath({ x: 1, y: 0 });
				board.endPath();

				board.startPath({ x: 1, y: 0 });
				board.continuePath({ x: 2, y: 0 });
				board.endPath();

				expect(board.grid[0][1])
					.toEqual(jasmine.objectContaining({ next: 'right' }));
			});

			it('should draw a path on to a matching destination', () => {
				withCorrectRedPath();

				expect(board.grid[2][1]).toEqual(jasmine.objectContaining({ next: 'left' }));
			});

			it('should not draw a path on to a wrong destination', () => {
				board.startPath({ x: 2, y: 2 });
				board.continuePath({ x: 1, y: 2 });
				board.continuePath({ x: 0, y: 2 });
				board.endPath();

				expect(board.grid[2][1]).toEqual(jasmine.objectContaining({ next: null }));
			});

			it('should not draw on to a destination with none 0 length', () => {
				(board.grid[0][0] as TileSource).length = 5;

				withCorrectRedPath();

				expect(board.grid[2][1]).toEqual(jasmine.objectContaining({ next: null }));
			});

			it('should decrease the length when drawing', () => {
				withCorrectRedPath();

				expect(board.grid[0][1]).toEqual(jasmine.objectContaining({ length: 3 }));
				expect(board.grid[2][1]).toEqual(jasmine.objectContaining({ length: 1 }));
			});

			it('should not be possible to draw a path beyond its length', () => {
				board.grid[0][0] = {
					type: 'source',
					color: 'red',
					next: null,
					length: 1,
				};

				withCorrectRedPath();

				expect(board.grid[0][1]).toEqual(jasmine.objectContaining({ length: 0, next: null, color: 'red' }));
				expect(board.grid[1][1]).toEqual(jasmine.objectContaining({ length: null, color: null, next: null }));
			});

			it('Should break the path', () => {
				withCorrectRedPath();

				board.startPath({ x: 1, y: 0 });
				board.endPath();

				expect(board.grid[0][0])
					.toEqual(
						jasmine.objectContaining({
							next: 'right',
							color: 'red',
						}),
					);
				expect(board.grid[0][1])
					.toEqual(
						jasmine.objectContaining({
							next: null,
							length: 3,
							color: 'red',
						}),
					);
				expect(board.grid[1][1])
					.toEqual(
						jasmine.objectContaining({
							next: null,
							length: null,
							color: null,
						}),
					);
			});

			it('Should not draw over another path', () => {
				withCorrectRedPath();

				board.startPath({ x: 2, y: 2 });
				board.continuePath({ x: 1, y: 2 });
				board.endPath();

				expect(board.grid[2][2])
					.toEqual(
						jasmine.objectContaining({
							next: null,
							color: 'green',
						}),
					);
				expect(board.grid[2][1])
					.toEqual(
						jasmine.objectContaining({
							color: 'red',
						}),
					);
			});
		});

		describe('Destination satisfaction', () => {
			it('should satisfy a Destination', () => {
				withCorrectRedPath();

				expect(board.grid[2][0]).toEqual(jasmine.objectContaining({ satisfied: true }));
			});

			it('should remove satisfied when breaking from a Destination', () => {
				withCorrectRedPath();

				board.startPath({ x: 1, y: 2 });
				board.endPath();

				expect(board.grid[2][0]).toEqual(jasmine.objectContaining({ satisfied: false }));
			});
		});

		describe('Is finished', () => {
			it('should finish', () => {
				withCorrectRedPath();

				board.startPath({ x: 2, y: 2 });
				board.continuePath({ x: 2, y: 1 });
				board.continuePath({ x: 2, y: 0 });
				board.endPath();

				expect(board.isFinished()).toBeTrue();
			});

			it('should not finish', () => {
				board.startPath({ x: 0, y: 0 });
				board.continuePath({ x: 1, y: 0 });
				board.continuePath({ x: 2, y: 0 });
				board.continuePath({ x: 2, y: 1 });
				board.continuePath({ x: 1, y: 1 });
				board.endPath();

				board.startPath({ x: 2, y: 2 });
				board.continuePath({ x: 1, y: 2 });
				board.continuePath({ x: 0, y: 2 });
				board.endPath();

				expect(board.isFinished()).toBeFalse();
			});
		});
	});
});
