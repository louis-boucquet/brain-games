import { Preset } from '../presets';
import { createGrid } from '../../../shared/helpers/grid-helpers';
import { LogicFlowBoard, Tile, TileSource } from './logic-flow-board';
import { colors } from '../../../shared/model/color';
import { Coordinate, createRandomCoordinate } from '../../../shared/model/coordinate';
import { comparatorBuilder, comparing } from 'sorting-lib';

export function generateBoard(
	{
		size,
		numDestinationTiles,
		numColors,
		numBlockedTiles,
	}: Preset['settings'],
) {
	console.log('Generating board', {
		size,
		numDestinationTiles,
		numColors,
		numBlockedTiles,
	});

	const grid = createGrid<Tile>(size, () => ({
		type: 'empty',
		next: null,
		length: null,
		color: null,
	}));

	let totalLength = (size.x * size.y) - numBlockedTiles - numColors;

	const colorConfigs = getColorConfigs(numColors, totalLength);

	for (const { color, length } of colorConfigs) {
		let colorSource: Coordinate;

		do colorSource = createRandomCoordinate(size);
		while (grid[colorSource.y][colorSource.x].type !== 'empty');

		grid[colorSource.y][colorSource.x] = {
			type: 'source',
			color,
			length,
			next: null,
		};
	}

	const logicFlowBoard = new LogicFlowBoard(grid, size);

	for (let i = 0; i < totalLength; i++) {
		const availableMoves = logicFlowBoard.getAvailableMoves();
		if (!availableMoves.length)
			break;

		const availableMove = availableMoves[Math.floor(Math.random() * availableMoves.length)];

		logicFlowBoard.startPath(availableMove.from);
		logicFlowBoard.continuePath(availableMove.to);
		logicFlowBoard.endPath();
	}

	for (const colorConfig of colorConfigs) {
		const tiles = grid.flatMap(row => row);

		const length = tiles
			.filter(tile =>
				tile.type === 'empty' &&
				tile.color === colorConfig.color)
			.length;

		tiles.find((tile): tile is TileSource =>
			tile.type === 'source' &&
			tile.color === colorConfig.color)!
			.length = length;
	}

	const destinationColors = colors.slice(0, numDestinationTiles);

	for (let y = 0; y < size.y; y++) {
		for (let x = 0; x < size.x; x++) {
			const tile = grid[y][x];
			if (
				tile.type === 'empty' &&
				tile.color !== null &&
				destinationColors.includes(tile.color) &&
				tile.next === null
			) {
				grid[y][x] = {
					type: 'destination',
					color: tile.color,
					satisfied: true,
				};
			}
		}
	}

	for (let y = 0; y < size.y; y++) {
		for (let x = 0; x < size.x; x++) {
			const tile = grid[y][x];
			if (tile.type === 'empty' && tile.color === null) {
				grid[y][x] = {
					type: 'blocked',
				};
			}
		}
	}

	for (let y = 0; y < size.y; y++) {
		for (let x = 0; x < size.x; x++) {
			const tile = grid[y][x];
			if (tile.type === 'source') {
				logicFlowBoard.startPath({ x, y });
				logicFlowBoard.endPath();
			}
		}
	}

	return logicFlowBoard;
}

function getColorConfigs(numColors: number, totalLength: number) {
	const colorConfigs = colors.slice(0, numColors).map(color => ({
		color,
		length: 0,
	}));
	for (let i = 0; i < totalLength; i++) {
		const index = Math.floor(Math.random() * colorConfigs.length);
		colorConfigs[index].length += 1;
	}

	colorConfigs.sort(comparatorBuilder<{ length: number }>()
		.add(comparing('length'))
		.invert()
		.build());
	return colorConfigs;
}
