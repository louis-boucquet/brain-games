import { Component, EventEmitter, Input, Output } from '@angular/core';
import { startWith, switchMap } from 'rxjs';
import { WithLifecycleHooks } from '../../../shared/with-lifecycle-hooks.component';
import { Coordinate, createRandomCoordinate } from '../../../shared/model/coordinate';

type GamePhase =
	| 'watching'
	| 'selecting'
	| 'finished'

@Component({
	selector: 'app-memory-game-board',
	templateUrl: './memory-game-board.component.html',
	styleUrls: ['./memory-game-board.component.scss'],
})
export class MemoryGameBoardComponent extends WithLifecycleHooks {
	@Input()
	gridSize!: Coordinate;

	@Input()
	numberOfTiles!: number;

	@Output()
	readonly onFinish = new EventEmitter<boolean>();

	coloredTiles: Coordinate[] = [];
	selectedTiles: Coordinate[] = [];

	gamePhase: GamePhase = 'watching';

	constructor() {
		super();

		this.onInit$
			.pipe(
				switchMap(() => this.onChanges$
					.pipe(startWith(undefined))),
			)
			.subscribe(() => this.initGameState());
	}

	selectTile(tileCoordinate: Coordinate) {
		if (this.gamePhase === 'selecting') {
			if (!this.isSelected(tileCoordinate))
				this.selectedTiles.push(tileCoordinate);

			if (this.selectedTiles.length === this.numberOfTiles) {
				this.gamePhase = 'finished';
				this.onFinish.emit(this.checkSelectedTiles());
			}
		}
	}

	isColored(c: Coordinate): boolean {
		return this.includesCoordinate(this.coloredTiles, c);
	}

	isSelected(c: Coordinate): boolean {
		return this.includesCoordinate(this.selectedTiles, c);
	}

	createRange(n: number): number[] {
		return new Array(n).map((_, i) => i);
	}

	private initGameState() {
		this.coloredTiles = [];
		this.selectedTiles = [];
		this.gamePhase = 'watching';

		this.populateColoredTiles();

		setTimeout(() => this.gamePhase = 'selecting', 300 * this.numberOfTiles);
	}

	private includesCoordinate(coordinates: Coordinate[], c: Coordinate) {
		return coordinates.some((tile) => tile.x === c.x && tile.y === c.y);
	}

	private populateColoredTiles() {
		for (let i = 0; i < this.numberOfTiles; i++) {
			let randomPoint: Coordinate;

			do randomPoint = createRandomCoordinate(this.gridSize);
			while (this.isColored(randomPoint));

			this.coloredTiles.push(randomPoint);
		}
	}

	private checkSelectedTiles() {
		return this.selectedTiles.every(tile => this.isColored(tile));
	}
}
