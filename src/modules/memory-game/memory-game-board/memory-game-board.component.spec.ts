import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemoryGameBoardComponent } from './memory-game-board.component';

describe('MemoryGameComponent', () => {
	let component: MemoryGameBoardComponent;
	let fixture: ComponentFixture<MemoryGameBoardComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [MemoryGameBoardComponent],
		})
			.compileComponents();

		fixture = TestBed.createComponent(MemoryGameBoardComponent);
		component = fixture.componentInstance;

		component.gridSize = { x: 3, y: 3 };
		component.numberOfTiles = 2;

		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
