import { Component } from '@angular/core';
import { WithLifecycleHooks } from '../../shared/with-lifecycle-hooks.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
	selector: 'app-memory-game',
	templateUrl: './memory-game.component.html',
	styleUrls: ['./memory-game.component.css'],
})
export class MemoryGameComponent extends WithLifecycleHooks {
	readonly formGroup = new FormGroup({
		level: new FormControl(null, Validators.required),
	});

	numberOfTiles = 3;
	gridSize = this.calculateGridSize();
	gameRunning = true;
	lastGameResult: boolean | null = null;

	onFinish(isSuccess: boolean) {
		this.lastGameResult = isSuccess;
		this.gameRunning = false;
	}

	startGame() {
		this.gameRunning = true;

		if (this.lastGameResult) this.levelUp();
		else this.levelDown();
	}

	goToLevel() {
		this.formGroup.markAllAsTouched();

		if (this.formGroup.invalid)
			return;
		const level = this.formGroup.value.level;

		if (!level)
			throw new Error('Level is not defined');

		this.numberOfTiles = level;
		this.gridSize = this.calculateGridSize();

		this.formGroup.reset();
	}

	private levelUp() {
		this.numberOfTiles++;
		this.gridSize = this.calculateGridSize();
	}

	private levelDown() {
		this.numberOfTiles--;
		this.gridSize = this.calculateGridSize();
	}

	private calculateGridSize() {
		const spaciousnessFactor = 3;

		return {
			x: Math.max(1, Math.floor(Math.sqrt(this.numberOfTiles * spaciousnessFactor))),
			y: Math.max(1, Math.ceil(Math.sqrt(this.numberOfTiles * spaciousnessFactor))),
		};
	}
}
