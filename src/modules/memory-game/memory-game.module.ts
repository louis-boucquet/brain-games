import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MemoryGameBoardComponent } from './memory-game-board/memory-game-board.component';
import { MemoryGameComponent } from './memory-game.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from '../../shared/modules/custom-forms/custom-forms.module';

@NgModule({
	declarations: [MemoryGameBoardComponent, MemoryGameComponent],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: MemoryGameComponent,
			},
		]),
		ReactiveFormsModule,
		CustomFormsModule,
	],
})
export class MemoryGameModule {
}
